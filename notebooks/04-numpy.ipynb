{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Une introduction à Numpy\n",
    "\n",
    "![](fig/python-logo.png)\n",
    "\n",
    "- Tableaux Numpy\n",
    "- Création de tableaux\n",
    "- Opérations basiques\n",
    "- Indexation et slicing\n",
    "- Algèbre linéaire\n",
    "- Méthodes sur les tableaux\n",
    "- Broadcasting de tableaux\n",
    "\n",
    "***\n",
    "\n",
    "*Contenu sous licence [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0), largement inspiré de <https://github.com/pnavaro/python-notebooks> et <https://github.com/jakevdp/PythonDataScienceHandbook>*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Numpy\n",
    "\n",
    "Le Python pur est peu performant pour le calcul. Les listes ne sont pas des objets efficaces pour représenter les tableaux numériques de grande taille. [Numpy](http://www.numpy.org/) a été créé à l'initiative de développeurs qui souhaitaient combiner la souplesse du langage python et des outils de calcul algébrique performants."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Numpy se base sur :\n",
    "\n",
    "- le `ndarray` : tableau multidimensionnel\n",
    "- des objets dérivés comme les *masked arrays* et les matrices\n",
    "- les `ufunc`s : opérations mathématiques optimisées pour les tableaux\n",
    "- des méthodes pour réaliser des opération rapides sur les tableaux :\n",
    "    - manipulation des *shapes*\n",
    "    - tri\n",
    "    - entrées/sorties\n",
    "    - FFT\n",
    "    - algébre linéaire\n",
    "    - statistiques\n",
    "    - calcul aléatoire\n",
    "    - et bien plus !\n",
    "  \n",
    "Numpy permet de calculer *à la Matlab* en Python. Il est un élément de base de l'écosystème [SciPy](https://www.scipy.org/)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Démarrer avec Numpy\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "print(np.__version__)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "Utilisez l'auto-complétion pour lister les objets numpy :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "#np.nd<TAB>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "La rubrique d'aide est également précieuse"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "np.ndarray?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Tableaux Numpy\n",
    "\n",
    "### Une question de performance"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "- Les listes Python sont trop lentes pour le calcul et utilisent beaucoup de mémoire\n",
    "- Représenter des tableaux multidimensionnels avec des listes de listes devient vite brouillon à programmer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Exemple avec le produit scalaire entre les vecteurs **a** et **b** :\n",
    "\n",
    "- en python pur avec des listes :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "from random import random\n",
    "from operator import mul\n",
    "\n",
    "a = [random() for i in range(100000)]\n",
    "b = [random() for i in range(100000)]\n",
    "%timeit s = sum(map(mul, a, b))\n",
    "print(sum(map(mul, a, b)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- en numpy avec des tableaux 1D :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "a_np = np.array(a)\n",
    "b_np = np.array(b)\n",
    "%timeit s = a_np.dot(b_np)\n",
    "print(a_np.dot(b_np))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### La différence avec les listes\n",
    "\n",
    "Les différences entre tableaux Numpy et listes Python :\n",
    "- un `ndarray` a une taille fixée à la création\n",
    "- un `ndarray` est composé d'éléments du même type\n",
    "- les opérations sur les tableaux sont réalisées par des routines C pré-compilées et optimisées (éventuellement parallèles)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "a = np.array([0, 1, 2, 3])  # list\n",
    "b = np.array((4, 5, 6, 7))  # tuple\n",
    "c = np.matrix('8 9 0 1')    # string (syntaxe matlab)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "print(a, b, c)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Propriétés"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "a = np.array([1, 2, 3, 4, 5])  # On crée un tableau"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "type(a)  # On vérifie son type"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "a.dtype  # On affiche le type de ses éléments"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "a.itemsize  # On affiche la taille mémoire (en octets) de chaque élément"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "a.shape  # On retourne un tuple indiquant la longueur de chaque dimension"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "a.size  #  On retourne le nombre total d'éléments"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "a.ndim   # On retourne le nombre de dimensions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "a.nbytes  # On retourne l'occupation mémoire"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "> - Toujours utiliser `shape` ou `size` pour les tableaux numpy plutôt que `len`\n",
    "> - `len` est réservé aux tableaux 1D"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Création de tableaux Numpy\n",
    "\n",
    "### Avec des valeur constantes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "x = np.zeros((5, 3))  # On ne précise pas le type : on crée des flottants\n",
    "print(x)\n",
    "print(x.dtype)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "x = np.zeros((5, 3), dtype=int)  # On explicite le type\n",
    "print(x)\n",
    "print(x.dtype)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "On dispose d'une panoplie de fonctions pour allouer des tableaux avec des valeurs constantes ou non initialisées (`empty`) :\n",
    "\n",
    "`empty, empty_like, ones, ones_like, zeros, zeros_like, full, full_like`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Création à partir d'une séquence\n",
    "\n",
    "\n",
    "#### `arange`\n",
    "\n",
    "C'est l'équivalent de `range` pour les listes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "np.arange(5)  # entiers de 0 à 4"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "np.arange(5, dtype=np.double)  # flottants de 0. à 4."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "np.arange(2, 7)  # entiers de 2 à 6."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "np.arange(2, 7, 0.5)  # flottants avec incrément de 0.5."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### `linspace` et `logspace`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# 5 éléments régulièrement espacés entre 1 et 4, 1 et 4 inclus\n",
    "np.linspace(1, 4, 5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# 5 éléments régulièrement espacés selon une progression géométrique entre 10^1 et 10^4\n",
    "np.logspace(1, 4, 5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consulter l'aide contextuelle pour plus de fonctionnalités"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.logspace?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Exercice : créer les tableaux suivants"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "```python\n",
    "[100 101 102 103 104 105 106 107 108 109]\n",
    "```\n",
    "> Astuce : `np.arange()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "solution2": "shown",
    "solution2_first": true
   },
   "outputs": [],
   "source": [
    "# Votre code ici"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "solution2": "hidden"
   },
   "outputs": [],
   "source": [
    "# Solution\n",
    "np.arange(100, 110)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "```python\n",
    "[-2. -1.8 -1.6 -1.4 -1.2 -1. -0.8 -0.6 -0.4 -0.2 0. \n",
    "0.2 0.4 0.6 0.8 1. 1.2 1.4 1.6 1.8]\n",
    "```\n",
    "\n",
    "> Astuce : `np.linspace()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "solution2": "hidden",
    "solution2_first": true
   },
   "outputs": [],
   "source": [
    "# Votre code ici"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "solution2": "hidden"
   },
   "outputs": [],
   "source": [
    "# Décommenter la ligne suivante pour voir la solution\n",
    "# %load exos/snippets/04_numpy/linspace.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "```python\n",
    "[ 0.001\t0.00129155 0.0016681 0.00215443 0.00278256 \n",
    "  0.00359381 0.00464159 0.00599484 0.00774264 0.01]\n",
    "```\n",
    "\n",
    "> Astuce : `np.logspace()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "solution2": "shown",
    "solution2_first": true
   },
   "outputs": [],
   "source": [
    "# Votre code ici"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "solution2": "hidden"
   },
   "outputs": [],
   "source": [
    "# Décommenter la ligne suivante pour voir la solution\n",
    "# %load exos/snippets/04_numpy/logspace.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "```python\n",
    "[[ 0. 0. -1. -1. -1.] \n",
    " [ 0. 0.  0. -1. -1.] \n",
    " [ 0. 0.  0.  0. -1.]\n",
    " [ 0. 0.  0.  0.  0.]\n",
    " [ 0. 0.  0.  0.  0.] \n",
    " [ 0. 0.  0.  0.  0.] \n",
    " [ 0. 0.  0.  0.  0.]]\n",
    "```\n",
    "\n",
    "> Astuce : `np.tri()`, `np.ones()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "solution2": "hidden",
    "solution2_first": true
   },
   "outputs": [],
   "source": [
    "# Votre code ici"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "solution2": "hidden"
   },
   "outputs": [],
   "source": [
    "# Décommenter la ligne suivante pour voir la solution\n",
    "# %load exos/snippets/04_numpy/tri.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Création de tableaux à partir de fichiers\n",
    "\n",
    "Afin d'illustrer la création d'un tableau numpy à partir de données lues dans un fichier texte, on commence par sauvegarder un tableau dans un fichier."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "x = np.arange(0.0, 5.0, 1.0)\n",
    "y = x * 10\n",
    "z = x * 100"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "np.savetxt('test.out', (x, y, z))\n",
    "%pycat test.out"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "np.savetxt('test.out', (x, y, z), fmt='%1.4e')   # Notation exponentielle\n",
    "%pycat test.out"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "np.loadtxt('test.out')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "`savetxt` et `loadtxt` ont leurs correspondants binaires :\n",
    "\n",
    "- [`save`](https://docs.scipy.org/doc/numpy-1.13.0/reference/generated/numpy.save.html#numpy.save) : enregistrer un tableau dans un fichier binaire au format `.npy`\n",
    "- [`load`](https://docs.scipy.org/doc/numpy-1.13.0/reference/generated/numpy.load.html#numpy.load): créer un tableau numpy à partir d'un fichier binaire numpy"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Format HDF5 avec H5py\n",
    "\n",
    "Le format `.npy` n'est lisible que par Numpy. \n",
    "À l'inverse, le format HDF5 est partagé par un grand nombre d'applications.\n",
    "De plus, il permet de structurer des données binaires :\n",
    "- en les nommants\n",
    "- en ajoutant des métadonnées\n",
    "- en assurant une portabilité indépendante de la plateforme\n",
    "\n",
    "> voir le [manuel utilisateur](http://docs.h5py.org)\n",
    "\n",
    "H5py est une interface Python pour HDF5."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "On écrit dans le fichier `test.h5`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "import h5py as h5\n",
    "\n",
    "with h5.File('test.h5', 'w') as f:\n",
    "    f['x'] = x\n",
    "    f['y'] = y\n",
    "    f['z'] = z"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "On lit le fichier `test.h5` (qui pourrait provenir d'une autre application)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "with h5.File('test.h5', 'r') as f:\n",
    "    for field in f.keys():\n",
    "        print(f\"{field}: {f[field][()]}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Opérations basiques sur les tableaux\n",
    "\n",
    "Par défaut, Numpy réalise les opérations arithmétiques éléments par éléments"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "a = np.array([0, 1, 2, 3])\n",
    "b = np.array((4, 5, 6, 7))\n",
    "\n",
    "print(a * b)  # Produit éléments par éléments : pas le produit matriciel !\n",
    "print(a + b)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(a**2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "print(5 * a)\n",
    "print(5 + a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "print(a < 2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(np.cos(a*np.pi))  # Utilisation de ufunc"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "De nombreuses ufunc dans la [doc officielle](https://docs.scipy.org/doc/numpy/user/quickstart.html#universal-functions)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "##  Indexation et slicing\n",
    "\n",
    "### Indexation\n",
    "\n",
    "Les règles différent légèrement des listes pour les tableaux multi-dimensionnels"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Indexation d'un tableau numpy\n",
    "a = np.arange(9).reshape(3, 3)  # Notez l'effet de la méthode reshape()\n",
    "print(a)\n",
    "print(a[1, 2])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Indexation de la liste équivalente\n",
    "liste = a.tolist()\n",
    "print(liste)\n",
    "print(liste[1][2])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Slicing\n",
    "\n",
    "Pour les tableaux unidimensionnels, les règles de slicing sont les mêmes que pour les séquences.\n",
    "Pour les tableaux multidimensionnels numpy, le slicing permet d'extraire des séquences dans n'importe quelle direction."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(a)\n",
    "print(a[2, :])  # Retourne la 3ème ligne\n",
    "print(a[:, 1])  # Retourne la deuxième colonne"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "**Attention :** contrairement aux listes, le slicing de tableaux ne renvoie pas une copie mais constitue une ***vue*** du tableau."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b = a[:, 1]\n",
    "b[0] = -1\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "`a` est aussi une vue du tableau `np.arange(9)` obtenue avec la méthode `reshape()` donc `a` et `b` sont deux vues différentes du même tableau :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b.base is a.base  # b.base retourne le tableau dont b est la vue"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Si on veut réaliser une copie d'un tableau, il faut utiliser la fonction `copy()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b = a[:, 1].copy()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "ici `b` n'est pas une vue mais une copie donc `b.base` vaut `None` et `a` n'est pas modifié."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(b.base)\n",
    "b[0] = 200\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Exercice\n",
    "\n",
    "Approcher la dérivée de $f(x) = \\sin(x)$ par la méthode des différences finies centrées :\n",
    "\n",
    "$$\\frac{\\partial f}{\\partial x} \\approx \\frac{f \\left(x + \\frac{\\Delta x}{2}\\right)-f \\left(x - \\frac{\\Delta x}{2} \\right)}{\\Delta x}$$\n",
    "\n",
    "Les valeurs seront calculées au milieu de deux abscisses discrètes successives."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# On crée un tableau de 40 points d'abscisse\n",
    "x, dx = np.linspace(0., 4.*np.pi, 40, retstep=True)\n",
    "y = np.sin(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- `x` représente les abscisses discrètes $x_i$ sur l'intervalle $x_i \\in [0, 4\\pi]$ et `dx` le pas de discrétisation.\n",
    "- `y` représente les valeurs de la fonction $f(x_i)$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "1. Calculer les valeurs de `x_mil` correspondant au milieu de l'intervale $[x_i, x_{i+1}]$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Votre code ici"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "2. Calculer les valeurs de `dy_dx` correspondant à la dérivée de `y` par différences finies centrées."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Votre code ici"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "3. Tracer la solution analytique $\\cos (x)$ et `dy_dx` en fonction de `x_mil`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "solution2": "hidden",
    "solution2_first": true
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "plt.title(\"Dérivée de sin(x)\")\n",
    "plt.xlabel(r\"x\")\n",
    "plt.ylabel(r\"$\\frac{\\partial f}{\\partial x}$\");\n",
    "# Décommentez les lignes ci-dessous pour tracer la dérivée numérique dy_dx\n",
    "# plt.plot(x_mil, np.cos(x_mil), label=\"cos(x)\")  # la dérivée analytique de sin() est cos()\n",
    "# plt.plot(x_mil, dy_dx, 'rx', label=\"différences finies\")  # x_mil est le milieu de deux abscisses\n",
    "plt.legend();"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    },
    "solution2": "hidden"
   },
   "outputs": [],
   "source": [
    "# Décommenter la ligne suivante pour voir la solution complète\n",
    "# %load exos/snippets/04_numpy/diff_finies.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Quelques opérations d'algèbre linéaire"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "A = np.array([[1, 1],\n",
    "              [0, 1]])\n",
    "B = np.array([[2, 0],\n",
    "              [3, 4]])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Produit élément par élément\n",
    "print(A * B)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Produit matriciel (3 syntaxes équivalentes)\n",
    "print(A.dot(B))      \n",
    "print(np.dot(A, B))\n",
    "print(A @ B)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Soit la matrice :\n",
    "$$\n",
    "A = \\begin{pmatrix}\n",
    "1 & 2 \\\\\n",
    "3 & 4\n",
    "\\end{pmatrix}\n",
    "$$ "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "a = np.array([[1., 2.],\n",
    "              [3., 4.]])\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Deux syntaxes équivalentes pour la transposition\n",
    "print(a.transpose())\n",
    "print(a.T)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "print(np.linalg.inv(a))  # inversion de matrice\n",
    "print(np.linalg.inv(a) @ a)  # on vérifie que l'on obtient la matrice identitée"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "print(np.trace(a))  # trace"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "print(np.eye(2))     # \"eye\" représente \"I\", la matrice identité"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "y = np.array([[5.0], [7.0]])  # Résoudre A*x = y\n",
    "x = np.linalg.solve(a, y)\n",
    "print(x)\n",
    "print(a @ x == y)  # On vérifie que x est solution"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "j = np.array([[0.0, -1.0],\n",
    "              [1.0, 0.0]])\n",
    "print(np.dot(j, j))  # produit matriciel\n",
    "print(np.linalg.eig(j))  # Extraction des valeurs propres"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Les méthodes associées aux tableaux"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "Elles sont très nombreuses : impossible de toutes les lister dans le cadre de ce cours.\n",
    "Citons brièvement :\n",
    "\n",
    "- `min`, `max`, `sum`\n",
    "- `sort`, `argmin`, `argmax`\n",
    "- statistiques basiques : `cov`, `mean`, `std`, `var`\n",
    "\n",
    "À chercher dans la [doc officielle](https://docs.scipy.org/doc/numpy/user/quickstart.html#)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Broadcasting de tableaux\n",
    "\n",
    "Le *broadcasting* est un mécanisme qui permet de faire des opérations sur des tableaux de différentes tailles ou *shapes*. \n",
    "\n",
    "\n",
    "Pour rappel, les opérations sur les tableaux de même taille sont réalisées élément par élément :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "a = np.array([0, 1, 2])\n",
    "b = np.array([1, 1, 1])\n",
    "a + b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Si on remplace `b` par le scalaire `1`, numpy déclenche le mécanisme de broadcasting où ce scalaire est virtuellement transformé en tableau de même taille que `a` en répétant la valeur, ce qui aboutit au même résultat :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "a + 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Si on additionne maintenant un tableau 1D et un tableau 2D :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = np.ones((3, 3), dtype=int)\n",
    "print(A)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A + a"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "`a` est étendu suivant la 2e dimension jusqu'à couvrir la *shape* de `A`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Considérons maintenant un tableau ligne et un tableau colonne :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = np.arange(3)\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b = np.arange(3).reshape(3, 1)\n",
    "print(b)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a + b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`a` et `b` sont étendus simultanément pour couvrir la *shape* `(3, 3)`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Règles de broadcasting\n",
    "\n",
    "1. Si les deux tableaux n'ont pas la même dimension, la *shape* de celui avec la plus faible dimension est complétée avec des $1$ sur son bord gauche.\n",
    "2. Si les *shapes* des tableaux n'ont aucune valeur commune, le tableau dont la shape vaut $1$ est étendue jusqu'à la valeur de l'autre tableau.\n",
    "3. Si les tailles sont différentes et non-unitaires dans toutes les dimensions, alors le broadcasting est impossible et on lève une erreur."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Exemple 1 : on étend un tableau"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = np.ones((2, 3), dtype=int)\n",
    "a = np.arange(3)\n",
    "print(f\"A =\\n {A}\")\n",
    "print(f\"a = {a}\")\n",
    "print(f\"{A.shape = }\")\n",
    "print(f\"{a.shape = }\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Selon la règle 1., la *shape* de `a` devient `(1, 3)`.\n",
    "- Selon la règle 2., la première dimension de `a` est étendue à `(2, 3)`.\n",
    "\n",
    "Maintenant que les *shapes* correspondent, l'addition terme à terme est possible :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A + a"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Exemple 2  : on étend les deux tableaux"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = np.arange(3).reshape((3, 1))\n",
    "print(a)\n",
    "print(f\"{a.shape = }\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b = np.arange(3)\n",
    "print(b)\n",
    "print(f\"{b.shape = }\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dans ce cas, les deux tableaux doivent être étendus.\n",
    "\n",
    "- Selon la règle 1., la *shape* de `b` doit être étendue en `(1, 3)`.\n",
    "- Selon la règle 2., on étend la *shape* de `a` et `b` en prenant le maximum de la taille dans chaque dimension soit `(3, 3)`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a + b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Exemple 3 :  broadcasting impossible"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = np.ones((3, 2), dtype=int)\n",
    "print(a)\n",
    "print(f\"{a.shape = }\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b = np.arange(3)\n",
    "print(b)\n",
    "print(f\"{b.shape = }\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Selon la règle 1., la *shape* de `b` devient `(1, 3)`.\n",
    "- Selon la règle 2., la *shape* de `b` devient `(3, 3)`.\n",
    "- Selon la règle 3., le broadcasting est alors impossible :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a + b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Exemple d'utilisation\n",
    "\n",
    "Un cas classique d'utilisation du broadcasting consiste à retirer la moyenne dans certaines dimensions :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rng = np.random.default_rng(seed=0)\n",
    "X = rng.random((6, 3))\n",
    "X"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On calcule la moyenne selon la première dimension :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Xmean = X.mean(0)\n",
    "Xmean"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "On peut maintenant calculer la valeur centrée de `X` en retirant la moyenne :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_centered = X - Xmean\n",
    "X_centered   "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On vérifie que `X_centered` est à moyenne nulle suivant la première dimension :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_centered.mean(0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Programmation avec Numpy\n",
    "\n",
    "- Les opérations sur les tableaux sont rapides, les boucles python sont lentes => **Éviter les boucles !**\n",
    "- C'est une gymnastique qui nécessite de l'entraînement\n",
    "- Le résultat peut devenir difficile à lire et à débugguer, par exemple dans le cas de boucles contenant de multiples conditions\n",
    "- Dans certains cas, l'écriture de boucles est inévitable : des solutions performantes existent comme Cython, Pythran, Numba, etc."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Références\n",
    "\n",
    "- [NumPy reference](http://docs.scipy.org/doc/numpy/reference/)\n",
    "- [Numpy by Konrad Hinsen](http://calcul.math.cnrs.fr/Documents/Ecoles/2013/python/NumPy%20avance.pdf)\n",
    "- [Cours de Pierre Navaro](https://github.com/pnavaro/python-notebooks)\n",
    "- [Python Datascience Handbook](https://github.com/jakevdp/PythonDataScienceHandbook), par Jake VanderPlas\n",
    "- [Scipy Lecture notes](https://lectures.scientific-python.org/\n",
    ")"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Format de la Cellule Texte Brut",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.7"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": false,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
