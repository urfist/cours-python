#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Process weather forecast json files to plot the time evolution of temperature
"""

import urllib.request
import json
import matplotlib.pyplot as plt
import sys
import numpy as np

URL_PREFIX = "https://www.prevision-meteo.ch/services/json/"


class City:
    """Loads a json dictionary downloaded from city name
    and provide a method to search for day temperature"""

    def __init__(self, name: str):
        """Loads city json dict"""
        self.name = name
        self.json = self._get_json(URL_PREFIX + name)

    def _get_json(self, jsonfile_url):
        """Download json file from URL and return a python dict"""
        f = urllib.request.urlopen(jsonfile_url)  # open url
        json_dict = json.loads(f.read())
        if 'errors' in json_dict:
            error = json_dict['errors'][0]
            msg = f"""\
Error for {self.name} (code: {error['code']})
{error['text']}
{error['description']}"""
            sys.exit(msg)
        else:
            return json_dict

    def get_temperature(self, day_key: str) -> tuple[np.array, np.array]:
        """return hour and temperature as numpy arrays for given day_key"""
        day = self.json[day_key]
        day_hd = day['hourly_data']  # point to hourly data
        tempe = [[int(hour[:-3]), data['TMP2m']] for hour, data in
                 day_hd.items()]  # Create a list of [hour, temperature]
        tempe.sort()  # Sort temperatures according to the hour of day
        t = np.array(tempe) # Create a numpy array
        return t[:, 0], t[:, 1]


def plot_day_temperature(*cities: City, day_number: int = 0):
    """Plot temperature vs hour for an arbitrary number of cities at given
    day_number (0: today, 1: tomorrow, etc.)"""

    fig, ax = plt.subplots()  # initialize a plot area

    day_key = f'fcst_day_{day_number}'


    # TODO: Uncomment above when the above is ready
    day = cities[0].json[day_key]

    title = f"{day['day_long']} {day['date']}"
    fig.suptitle(title, fontsize=14, fontweight='bold')

    fig.subplots_adjust(top=0.85)
    ax.set_title('Evolution horaire')
    ax.set_xlabel('Temps [h]')
    ax.set_ylabel('Température [deg. C]')

    for city in cities:
        hour, temperature = city.get_temperature(day_key)
        ax.plot(hour, temperature, label=city.name)

    ax.legend()  # Add legend to plot
    plt.show()  # Ensure figure is shown


if __name__ == '__main__':
    # This block is not executed if this file is imported as a module

    # Instanciate City object:
    toulouse = City('Toulouse')
    paris = City('Paris')
    strasbourg = City('Strasbourg')
    # Plot temperature evolution of all these objects on the same graph
    plot_day_temperature(toulouse, paris, strasbourg, day_number=0)
