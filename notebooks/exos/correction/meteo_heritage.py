#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Process weather forecast json files to plot the time evolution of temperature
"""

import geocoder
from meteo_city import City, plot_day_temperature

URL_PREFIX = "https://www.prevision-meteo.ch/services/json/"


class Location(City):
    """Loads a json dictionary downloaded from geographic coordinates"""

    def __init__(self, lat, lng):
        jsonfile_url = f"{URL_PREFIX}lat={lat}lng={lng}"
        self.name = f"lat={lat}, lng={lng}"
        self.json = self._get_json(jsonfile_url)


class Here(Location):
    """Loads a json dictionary downloaded from current position"""

    def __init__(self):
        g = geocoder.ip("me")
        super().__init__(*g.latlng)
        self.name = f"here ({g.city})"


if __name__ == "__main__":
    # This block is not executed if this file is imported as a module

    # Instanciate City and Location objects:
    toulouse = City("Toulouse")
    paris = City("Marseille")
    strasbourg = City("Strasbourg")
    trou_perdu = Location(lat=45.32, lng=10)
    here = Here()
    # Plot temperature evolution of all these objects on the same graph
    plot_day_temperature(toulouse, paris, strasbourg, trou_perdu, here, day_number=0)
