"""
Process a weather forecast json file to plot the time evolution of today's
temperature in Strasbourg
"""

from urllib.request import urlopen
import json
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import ssl
from io import BytesIO

# Ignore SSL certificate errors
context = ssl._create_unverified_context()

jsonfile_url = f"https://www.prevision-meteo.ch/services/json/Strasbourg"

try:
    f = urlopen(jsonfile_url, timeout=10, context=context)  # open url
except Exception:
    print("Le téléchargement a échoué : on lit une version locale.")
    f = open("exos/Strasbourg.json")

jsondict = json.loads(f.read())  # Read JSON file
f.close()  # close file

day = jsondict["fcst_day_0"]  # point the current day data
day_hd = day["hourly_data"]  # point to hourly data

# Get hours and temperature lists
hours = []
temperatures = []
for hour, data in day_hd.items():
    # get first part of time in "00H00" format and remove "H00"
    hours.append(int(hour[:-3]))
    # get temperature at 2m above ground 'TMP2m'
    temperatures.append(data["TMP2m"])

# Plot T = T(hour)
fig = plt.figure()  # initialise figure
title = f'{day["day_long"]} {day["date"]}'
fig.suptitle(title, fontsize=14, fontweight="bold")

ax = fig.add_subplot(111)  # initialise a plot area
fig.subplots_adjust(top=0.85)

ax.set_title('Évolution horaire de la température')
ax.set_xlabel('Heure [h]')
ax.set_ylabel('Température [dég. C]')

ax.plot(hours, temperatures)  # plot temperatures vs hours

# Add meteo icon to plot
# Open weather icon:
icon = BytesIO(urlopen(day["icon_big"], context=context).read())

axicon = fig.add_axes([0.8, 0.8, 0.15, 0.15])  # Add axes for icon
img = mpimg.imread(icon)  # initialize image
axicon.set_xticks([])  # Remove axes ticks
axicon.set_yticks([])
axicon.imshow(img)  # trigger the image show

plt.show()  # trigger the figure display
