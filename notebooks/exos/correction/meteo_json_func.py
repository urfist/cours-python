# coding: utf-8
"""
Process a weather forecast json file to plot the time evolution of temperature
of a given day in a given city
"""

from urllib.request import urlopen
import json
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from io import BytesIO


def get_day_forecast(day_key: int, city_name: str) -> dict:
    """
    Get the time evolution of temperature for a given day in a given city

    Args:
        day_key: int
            The day to plot the temperature evolution
        city_name: str
            The city to get the weather forecast from

    Returns:
        day: dict
            The weather forecast for the given day
    """

    jsonfile_url = f"https://www.prevision-meteo.ch/services/json/{city_name}"

    try:
        f = urlopen(jsonfile_url, timeout=10)  # open url
    except Exception:
        print("Le téléchargement a échoué : on lit une version locale.")
        f = open("exos/Strasbourg.json")

    jsondict = json.loads(f.read())  # Read JSON file
    # Raise error if city does not exist
    if "errors" in jsondict:
        print(
            f"{jsondict['errors'][0]['text']}: {jsondict['errors'][0]['description']}"
        )
        exit(1)
    try:
        day = jsondict[f"fcst_day_{day_key:d}"]
    except KeyError:
        # Raise error if day key does not exist
        day_keys = tuple(
            int(fcst_key[-1])
            for fcst_key in jsondict.keys()
            if fcst_key.startswith("fcst_day_")
        )
        print(f"Error: day_key must be in {day_keys}")
        exit(1)
    return day


def get_hours_temperatures(day: dict) -> tuple[list[int], list[float]]:
    """
    Get the hours and temperatures from a weather forecast day

    Args:
        day: dict
            The weather forecast for a given day

    Returns:
        hours: list
            The hours of the day
        temperatures: list
            The temperature at each hour
    """
    day_hd = day["hourly_data"]

    # Get hours and temperature lists
    hours = []
    temperatures = []
    for hour, data in day_hd.items():
        # get first part of time in "00H00" format and remove "H00"
        hours.append(int(hour[:-3]))
        # get temperature at 2m above ground 'TMP2m'
        temperatures.append(data["TMP2m"])

    return hours, temperatures


def plot(
    hours: list[int], temperatures: list[float], city_name: str, day: dict
):
    """
    Plot the time evolution of predicted temperature

    Args:
        hours: list
            The hours of the day
        temperatures: list
            The temperature at each hour
        city_name: str
            The city name
        day: dict
            The weather forecast for a given day
    """

    # Plot T = T(hour)
    fig, ax = plt.subplots()  # initialize figure and plot area
    title = f"{day['day_long']} {day['date']} à {city_name.title()}"
    fig.suptitle(title, fontsize=14, fontweight="bold")
    fig.subplots_adjust(top=0.85)

    ax.set_title("Évolution horaire de la température")
    ax.set_xlabel("Heure [h]")
    ax.set_ylabel("Température [dég. C]")

    ax.plot(hours, temperatures)  # plot temperatures vs hours

    # Add meteo icon to plot
    icon = BytesIO(urlopen(day["icon_big"]).read())  # Open weather icon

    axicon = fig.add_axes([0.8, 0.8, 0.15, 0.15])
    img = mpimg.imread(icon)  # initialise image
    axicon.set_xticks([])  # Remove axes ticks
    axicon.set_yticks([])
    axicon.imshow(img)  # trigger the image show

    plt.show()  # trigger the figure display


def main(day_key: int = 0, city_name: str = "strasbourg"):
    """Plot the time evolution of predicted temperature"""

    day = get_day_forecast(day_key, city_name)
    hours, temperatures = get_hours_temperatures(day)
    plot(hours, temperatures, city_name, day)


if __name__ == "__main__":
    # Create a CLI using https://github.com/google/python-fire
    import fire

    fire.Fire(main)
