from ..sous_paquet.deco import boule

def boule_or(*args, **kwargs):
    # seule instruction qui fait une hypothèse
    # sur la signature de la fonction boule() :
    kwargs['couleur'] = "or"
    return boule(*args, **kwargs)
