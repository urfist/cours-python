# 2. on construit le dictionnaire inverse icode = {codon: letter}
icode = {}
# on parcourt le dictionnaire code lettre par lettre
for letter, acide in code.items():
    # pour chaque codon de la liste acide['codons'], on ajoute le couple {codon: letter}
    for codon in acide["codons"]:
        icode[codon] = letter

# Ou en une seule ligne avec la compréhension de dictionnaire :
# icode = {codon: letter for letter, acide in code.items() for codon in acide['codons']}
