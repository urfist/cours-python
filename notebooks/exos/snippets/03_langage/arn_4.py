# 4. on lit la chaîne de caractères dans le fichier en enlevant le retour à ligne
arn_mystere = open('exos/arn.txt', 'r').read().strip()

print("Version symbole :", decode(arn_mystere))
print("Version abrv.   :", decode(arn_mystere, form='abrv'))
