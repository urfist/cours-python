# Solution
def openfile():
    filename = input('Donnez un chemin de fichier : ')
    try:
        with open(filename, 'r') as f:
            print(f.readline())
        return True
    except FileNotFoundError as err:
        # Ignore errors while trying to open file
        return False

print(openfile())
