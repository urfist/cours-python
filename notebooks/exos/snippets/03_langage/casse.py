# Solution

lower = "abcdefghijklmnopqrstuvwxyz"
upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

toupper = dict(zip(lower, upper))
tolower = dict(zip(upper, lower))

inverse = dict(toupper)
inverse.update(tolower)


def to_case(chaine: str, dico: dict) -> str:
    ret = []
    for c in chaine:
        d = dico.get(c)
        ret.append(d or c)
    return "".join(ret)


def majuscule(chaine: str) -> str:
    return to_case(chaine, toupper)


def minuscule(chaine: str) -> str:
    return to_case(chaine, tolower)


def inverse_casse(chaine: str) -> str:
    return to_case(chaine, inverse)


def nom_propre(chaine: str) -> str:
    return majuscule(chaine[0]) + minuscule(chaine[1:])


assert majuscule("azERtyUI") == "AZERTYUI"
assert minuscule("azERtyUI") == "azertyui"
assert inverse_casse("azERtyUI") == "AZerTYui"
assert nom_propre("azERtyUI") == "Azertyui"
