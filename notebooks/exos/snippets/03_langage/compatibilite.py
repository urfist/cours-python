# Solutions

# 1.
print(repr(str(a) + b))  # On utilise la fonction str()

# 2.
print(a + int(b)) # On convertit en entier

# 3.
print(int(str(a) + b)) # On fait les deux
