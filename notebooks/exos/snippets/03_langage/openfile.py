# Solution
def openfile():
    filename = input('Donnez un chemin de fichier : ').strip()
    try:
        f = open(filename, 'r')
        print(f.readline())
        f.close()
        return True
    except FileNotFoundError as err:
        # Ignore les erreurs lorsqu'on essaie d'ouvrir le fichier
        return False

print(openfile())
