# 3. on définit la fonction de décodage
def decode(arn: str, form: str = "letter") -> str:
    """Traduit la chaîne de caractère arn en sequence de peptides"""
    # On coupe la chaîne arn en une liste de sous-chaînes de 3 caractères (= codons)
    n = 3
    codons = [arn[i : i + n] for i in range(0, len(arn), n)]
    if form == "letter":
        # on utilise directement le dico icode: codon -> letter
        peptide = "".join(icode[codon] for codon in codons)
    elif form == "abrv":
        # on enchaîne les dicos icode et code pour résoudre : codon -> letter -> abrv
        peptide = "-".join(code[icode[codon]]["abrv"] for codon in codons)
    return peptide
