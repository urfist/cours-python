# Solution
def boule_or(*args, **kwargs):
    # seule instruction qui fait une hypothèse
    # sur la signature de la fonction boule() :
    kwargs['couleur'] = "or"
    return boule(*args, **kwargs)
boule_or(1, 2, rendu='brillant') 
