# Solution

# 1. on construit le dictionnaire code = {letter: {name: , abrv:, etc.}}
import csv

code = {}
with open("exos/code_genetique.csv", newline="") as csvfile:
    code_brut = csv.reader(csvfile, delimiter=";")
    for acide_amine in code_brut:
        lettre = acide_amine[1]
        code[lettre] = {
            "name": acide_amine[0],
            "abrv": acide_amine[2],
            "codons": acide_amine[3].split(","),
        }


# 2. on construit le dictionnaire inverse icode = {codon: letter}
icode = {codon: letter for letter, acide in code.items() for codon in acide["codons"]}


# 3. on définit la fonction de décodage
def decode(arn: str, form: str = "letter") -> str:
    """Traduit la chaîne de caractère arn en sequence de peptides"""
    # On coupe la chaîne arn en une liste de sous-chaînes de 3 caractères (= codons)
    n = 3
    codons = [arn[i : i + n] for i in range(0, len(arn), n)]
    if form == "letter":
        # on utilise directement le dico icode: codon -> letter
        peptide = "".join([icode[codon] for codon in codons])
    elif form == "abrv":
        # on enchaîne les dicos icode et code pour résoudre : codon -> letter -> abrv
        peptide = "-".join([code[icode[codon]]["abrv"] for codon in codons])
    return peptide


# 4. on lit la chaîne de caractères dans le fichier en enlevant le retour à ligne
arn_mystere = open("exos/arn.txt", "r").read().strip()

print("Version symbole :", decode(arn_mystere))
print("Version abrv.   :", decode(arn_mystere, form="abrv"))
