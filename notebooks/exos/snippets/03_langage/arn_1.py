# 1. on construit le dictionnaire code = {letter: {name: , abrv:, etc.}}
import csv

code = {}
with open("exos/code_genetique.csv", newline="") as csvfile:
    code_brut = csv.reader(csvfile, delimiter=";")
    for acide_amine in code_brut:
        lettre = acide_amine[1]
        code[lettre] = {
            "name": acide_amine[0],
            "abrv": acide_amine[2],
            "codons": acide_amine[3].split(","),
        }
