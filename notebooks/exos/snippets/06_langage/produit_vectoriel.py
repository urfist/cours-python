def __mul__(self, v: Vecteur) -> Vecteur:
    """Produit vectoriel"""
    x1, y1, z1 = self.coord
    x2, y2, z2 = v.coord
    xp = y1 * z2 - z1 * y2
    yp = z1 * x2 - x1 * z2
    zp = x1 * y2 - y1 * x2
    return Vecteur(xp, yp, zp)


Vecteur.__mul__ = __mul__

v1 = Vecteur(1.0, 0.0, 0.0)
v2 = Vecteur(0.0, 1.0, 0.0)
print(v1 * v2)
