from __future__ import annotations


class Vecteur:
    def __init__(self, *args: float):
        self.coord = list(args)
        self.ndim = len(args)

    def __repr__(self) -> str:
        return f"Vecteur({', '.join(str(i) for i in self.coord)})"

    def __str__(self) -> str:
        return "".join(f"({c})\n" for c in self.coord)

    def __add__(self, v: Vecteur) -> Vecteur:
        new_coord = [self.coord[i] + v.coord[i] for i in range(len(self.coord))]
        return Vecteur(*new_coord)

    def __sub__(self, v: Vecteur) -> Vecteur:
        new_coord = [self.coord[i] - v.coord[i] for i in range(len(self.coord))]
        return Vecteur(*new_coord)

    def __mul__(self, v: Vecteur) -> Vecteur:
        if self.ndim != 3:
            raise ValueError(
                "Le produit vectoriel n'est défini que pour les vecteurs de dimension 3"
            )
        return Vecteur(
            self.coord[1] * v.coord[2] - self.coord[2] * v.coord[1],
            self.coord[2] * v.coord[0] - self.coord[0] * v.coord[2],
            self.coord[0] * v.coord[1] - self.coord[1] * v.coord[0],
        )

    def __eq__(self, other: Vecteur) -> bool:
        return self.coord == other.coord


assert Vecteur(1.0, 0.0, 0.0) + Vecteur(0.0, 1.0, 0.0) == Vecteur(1.0, 1.0, 0.0)
assert Vecteur(1.0, 0.0, 0.0) * Vecteur(0.0, 1.0, 0.0) == Vecteur(0.0, 0.0, 1.0)

print(f"{Vecteur(1., 2.) + Vecteur(3., 4.) = }")
print(f"{Vecteur(1., 2., 3., 4.) + Vecteur(5., 6., 7., 8.) = }")
print(f"{Vecteur(1., 2., 3., 4.) - Vecteur(5., 6., 7., 8.) = }")
