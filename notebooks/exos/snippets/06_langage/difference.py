def __sub__(self, v: Vecteur) -> Vecteur:
    """
    Retourne une nouvelle instance Vecteur correspondant à la différence `self - v`

    Args:
        self: l'instance de Vecteur comme membre de gauche de l'opérateur `-`
        v: un autre objet Vecteur comme membre de droite
    """
    return Vecteur(
        self.coord[0] - v.coord[0],
        self.coord[1] - v.coord[1],
        self.coord[2] - v.coord[2],
    )


Vecteur.__sub__ = __sub__

Vecteur(1.0, 2.0, 3.0) - Vecteur(3.0, 2.0, 1.0)
