chaine_donnee = "azertyuiooiuytreza"

milieu = len(chaine_donnee) // 2
print(chaine_donnee[:milieu] + "#" + chaine_donnee[milieu:])

tiers = len(chaine_donnee) // 3
print(
    chaine_donnee[:tiers]
    + "@"
    + chaine_donnee[tiers : 2 * tiers]
    + "@"
    + chaine_donnee[2 * tiers :]
)

print("|".join(chaine_donnee))
