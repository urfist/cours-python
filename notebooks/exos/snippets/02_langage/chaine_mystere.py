import codecs

mystere = [
    "Gur Mra bs Clguba, ol Gvz Crgref\n\n",
    "Ornhgvshy vf orggre guna htyl.\n",
    "Rkcyvpvg vf orggre guna vzcyvpvg.\n",
]

# 1. On écrit ces chaînes de caractères dans le fichier "coded.txt"
f = open("coded.txt", mode="w")
f.writelines(mystere)
f.close()

# 2. On relit le contenu du fichier "coded.txt" que l'on vient de créer
f = open("coded.txt", mode="r")
coded = f.read()
f.close()

# 3. On décode le message mystérieux
# Cette ligne permet l'utilisation de codecs.decode(), c.f. ligne 21
decoded = codecs.decode(coded, encoding="rot13")

# 4. On écrit le message décodé dans un autre fichier
f = open("decoded.txt", mode="w")
f.write(decoded)
f.close()

# 5. On consulte le contenu du message décodé dans le fichier "decoded.txt"
