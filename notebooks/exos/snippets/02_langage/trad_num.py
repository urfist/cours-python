fr = ("un", "deux", "trois")
en = ("one", "two", "three")
num = (1, 2, 3)

trad_num = dict(zip(fr, en))
trad_num.update(zip(en, fr))
trad_num.update(zip(num, fr))

print(trad_num)
print(trad_num["one"], trad_num["trois"], trad_num[2])
