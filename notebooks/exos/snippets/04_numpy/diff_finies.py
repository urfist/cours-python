import numpy as np
import matplotlib.pyplot as plt

x, dx = np.linspace(0., 4.*np.pi, 40, retstep=True)
y = np.sin(x)
x_mil = 0.5*(x[1:] + x[:-1])
dy = y[1:] - y[:-1]
dy_dx = dy/dx
plt.plot(x_mil, np.cos(x_mil), label="cos(x)")  # la dérivée analytique de sin() est cos()
plt.plot(x_mil, dy_dx, 'rx', label="différences finies")  # x_mil est le milieu de deux abscisses
plt.title("Dérivée de sin(x)")
plt.legend()
plt.xlabel(r"x")
plt.ylabel(r"$\frac{\partial f}{\partial x}$");
